<?php
namespace App\Event;

use App\Entity\notification\Notification;
use Symfony\Contracts\EventDispatcher\Event;

class NotificationCompleteSaleEvent extends Event
{
    const NAME = 'notification.completesale';

    private Notification $notification;

    public function __construct(Notification $notification)
    {
        $this->notification = $notification;
    }

    public function getNotification(): Notification
    {
        return $this->notification;
    }
}