<?php
namespace App\Entity\user;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Entity\user\EmailTokenRepository")
 * @ORM\Table(name="email_token")
 */
class EmailToken {
    const TYPE_ACCOUNT_ACTIVATION = 0;
    const TYPE_LOST_PASSWORD = 1;

    /**
     * @ORM\Column(type="integer", name="id")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private ?int $id = null;

    /**
     * @return int|null
     */
    public function getId():?int{
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId(?int $id):void{
        $this->id = $id;
    }

    /**
     * @return int|null
     */
    public function getIdUser():?int{
        return $this->idUser;
    }

    /**
     * @param int|null $idUser
     */
    public function setIdUser(?int $idUser):void{
        $this->idUser = $idUser;
    }

    /**
     * @return string|null
     */
    public function getToken():?string{
        return $this->token;
    }

    /**
     * @param string|null $token
     */
    public function setToken(?string $token):void{
        $this->token = $token;
    }

    /**
     * @return DateTime|null
     */
    public function getExpiredAt():?DateTime{
        return $this->expiredAt;
    }

    /**
     * @param DateTime|null $expiredAt
     */
    public function setExpiredAt(?DateTime $expiredAt):void{
        $this->expiredAt = $expiredAt;
    }

    /**
     * @return int|null
     */
    public function getType():?int{
        return $this->type;
    }

    /**
     * @param int|null $type
     */
    public function setType(?int $type):void{
        $this->type = $type;
    }
    /**
     * @ORM\Column(type="integer", name="id_user", nullable=false)
     */
    private ?int $idUser = null;

    /**
     * @ORM\Column(type="string", name="token", length=255, nullable=false)
     */
    private ?string $token = null;

    /**
     * @ORM\Column(type="datetime", name="expired_at")
     */
    private ?DateTime $expiredAt = null;

    /**
     * @ORM\Column(type="integer", name="type", nullable=false)
     */
    private ?int $type = null;


    //--------- constructor ----------
    public function __construct(){
        $this->expiredAt = new DateTime();
        $this->expiredAt->modify('+60 minute');
        $this->token = uniqid();
    }
}
