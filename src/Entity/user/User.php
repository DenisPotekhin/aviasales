<?php

namespace App\Entity\user;

use App\Entity\IdUuidTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Entity\user\UserRepository")
 * @ORM\Table(name="user")
 */
class User {
    const NOT_ACTIVE = 0;
    const ACTIVE = 1;

    use IdUuidTrait;


    /**
     * @ORM\Column(type="string", name="login", length=20, nullable=false)
     */
    private ?string $login = null;
    /**
     * @ORM\Column(type="string", name="password", length=255, nullable=false)
     */
    private ?string $password = null;
    /**
     * @ORM\Column(type="string", name="email", length=80, nullable=false)
     */
    private ?string $email = null;
    /**
     * @ORM\Column(type="string", name="name", length=255, nullable=true)
     */
    private ?string $name = null;
    /**
     * @ORM\Column(type="string", name="surname", length=255, nullable=true)
     */
    private ?string $surname = null;


    /**
     * @return int|null
     */
    public function getIsActive():?int{
        return $this->isActive;
    }

    /**
     * @param int|null $isActive
     */
    public function setIsActive(?int $isActive):void{
        $this->isActive = $isActive;
    }
    /**
     * @ORM\Column(type="integer", name="is_active", nullable=true)
     */
    private ?int $isActive = self::NOT_ACTIVE;


    public function getLogin():?string{
        return $this->login;
    }

    public function setLogin(?string $login){
        $this->login = $login;
        return $this;
    }

    public function getPassword():?string{
        return $this->password;
    }

    public function setPassword(?string $password){
        $this->password = $password;
        return $this;
    }

    public function getEmail():?string{
        return $this->email;
    }

    public function setEmail(?string $email){
        $this->email = $email;
        return $this;
    }

    public function getName():?string{
        return $this->name;
    }

    public function setName(?string $name){
        $this->name = $name;
        return $this;
    }

    public function getSurname():?string{
        return $this->surname;
    }

    public function setSurname(?string $surname){
        $this->surname = $surname;
        return $this;
    }


    /**
     * method suitable for json response
     */
    public function toArray(){
        return [
            'name' => $this->name,
            'surname' => $this->surname,
            'uuid' => $this->uuid,
            'login' => $this->login,
            'email' => $this->email,
            'isActive' => $this->isActive
        ];
    }
}
