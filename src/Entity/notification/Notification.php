<?php
namespace App\Entity\notification;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Entity\notification\NotificationRepository")
 * @ORM\Table(name="notification")
 */
class Notification {
    const SALE_COMPLETE = 1;
    const FLIGHT_CANCEL = 2;

    /**
     * @return int|null
     */
    public function getId():?int{
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId(?int $id):void{
        $this->id = $id;
    }

    /**
     * @return int|null
     */
    public function getIdFlight():?int{
        return $this->idFlight;
    }

    /**
     * @param int|null $idFlight
     */
    public function setIdFlight(?int $idFlight):void{
        $this->idFlight = $idFlight;
    }

    /**
     * @return int|null
     */
    public function getType():?int{
        return $this->type;
    }

    /**
     * @param int|null $type
     */
    public function setType(?int $type):void{
        $this->type = $type;
    }

    /**
     * @return DateTime|null
     */
    public function getTriggeredAt():?DateTime{
        return $this->triggeredAt;
    }

    /**
     * @param DateTime|null $triggeredAt
     */
    public function setTriggeredAt(?DateTime $triggeredAt):void{
        $this->triggeredAt = $triggeredAt;
    }

    /**
     * @ORM\Column(type="integer", name="id")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="integer", name="id_flight", nullable=false)
     */
    private ?int $idFlight = null;

    /**
     * @ORM\Column(type="integer", name="type", nullable=false)
     */
    private ?int $type = null;

    /**
     * @ORM\Column(type="datetime", name="triggered_at")
     */
    private ?DateTime $triggeredAt = null;


    /**
     * method suitable for json response
     */
    public function toArray(){
        return [
            'id' => $this->getId(),
            'idFlight' => $this->getIdFlight(),
            'triggeredAt' => $this->getTriggeredAt()->getTimestamp(),
            'type'  => $this->getType() == self::SALE_COMPLETE ? 'Все билеты на рейс проданы' : 'Рейс отменен',
        ];
    }

}
