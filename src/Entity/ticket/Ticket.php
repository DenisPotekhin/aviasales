<?php
namespace App\Entity\ticket;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Entity\ticket\TicketRepository")
 * @ORM\Table(name="ticket")
 */
class Ticket {

    const STATE_DEFAULT = 0;
    const STATE_RESERVED = 1;
    const STATE_BOUGHT = 2;

    /**
     * @ORM\Column(type="integer", name="id")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private ?int $id = null;

    /**
     * @return int|null
     */
    public function getId():?int{
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId(?int $id):void{
        $this->id = $id;
    }

    /**
     * @return int|null
     */
    public function getIdFlight():?int{
        return $this->idFlight;
    }

    /**
     * @param int|null $idFlight
     */
    public function setIdFlight(?int $idFlight):void{
        $this->idFlight = $idFlight;
    }

    /**
     * @return int|null
     */
    public function getState():int{
        return $this->state;
    }

    /**
     * @param int $state
     */
    public function setState(int $state):void{
        $this->state = $state;
    }

    /**
     * @return int|null
     */
    public function getNumber():?int{
        return $this->number;
    }

    /**
     * @param int|null $number
     */
    public function setNumber(?int $number):void{
        $this->number = $number;
    }

    /**
     * @ORM\Column(type="integer", name="id_flight", nullable=false)
     */
    private ?int $idFlight = null;

    /**
     * @ORM\Column(type="integer", name="state", nullable=true)
     */
    private int $state = self::STATE_DEFAULT;

    /**
     * @ORM\Column(type="integer", name="number", nullable=false)
     */
    private ?int $number = null;

	
	/**
	 * method suitable for json response
	 */
	public function toArray(){
		return [
			'id' => $this->getId(),
			'idFlight' => $this->getIdFlight(),
			'number' => $this->getNumber(),
			'state'  => $this->getState(),
		];
	}

}
