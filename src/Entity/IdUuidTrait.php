<?php
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

trait IdUuidTrait
{
    /**
     * @ORM\Column(type="integer", name="id")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected ?int $id = null;

    /**
     * @ORM\Column(type="string", name="uuid", length=255, nullable=false)
     */
    protected ?string $uuid = null;

    //--- getters / setters ---
    public function getId():?int{
        return $this->id;
    }

    public function setId(?int $id){
        $this->id = $id;
        return $this;
    }

    public function getUuid():?string{
        return $this->uuid;
    }

    public function setUuid(?string $uuid){
        $this->uuid = $uuid;
        return $this;
    }

}
