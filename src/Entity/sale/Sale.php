<?php
namespace App\Entity\sale;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Entity\sale\SaleRepository")
 * @ORM\Table(name="sale")
 */
class Sale {
    const CANCELLATION_NOT = 0;
    const CANCELLATION_YES = 1;

    /**
     * @return int|null
     */
    public function getId():?int{
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId(?int $id):void{
        $this->id = $id;
    }

    /**
     * @return int|null
     */
    public function getIdUser():?int{
        return $this->idUser;
    }

    /**
     * @param int|null $idUser
     */
    public function setIdUser(?int $idUser):void{
        $this->idUser = $idUser;
    }

    /**
     * @return int|null
     */
    public function getIdTicket():?int{
        return $this->idTicket;
    }

    /**
     * @param int|null $idTicket
     */
    public function setIdTicket(?int $idTicket):void{
        $this->idTicket = $idTicket;
    }

    /**
     * @return DateTime|null
     */
    public function getCreatedAt():?DateTime{
        return $this->createdAt;
    }

    /**
     * @param DateTime|null $createdAt
     */
    public function setCreatedAt(?DateTime $createdAt):void{
        $this->createdAt = $createdAt;
    }

    /**
     * @return DateTime|null
     */
    public function getModifiedAt():?DateTime{
        return $this->modifiedAt;
    }

    /**
     * @param DateTime|null $modifiedAt
     */
    public function setModifiedAt(?DateTime $modifiedAt):void{
        $this->modifiedAt = $modifiedAt;
    }

    /**
     * @return int
     */
    public function getCancellation():int{
        return $this->cancellation;
    }

    /**
     * @param int $cancellation
     */
    public function setCancellation(int $cancellation):void{
        $this->cancellation = $cancellation;
    }

    /**
     * @ORM\Column(type="integer", name="id")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="integer", name="id_user", nullable=false)
     */
    private ?int $idUser = null;

    /**
     * @ORM\Column(type="integer", name="id_ticket", nullable=false)
     */
    private ?int $idTicket = null;

    /**
     * @ORM\Column(type="datetime", name="created_at")
     */
    private ?DateTime $createdAt = null;

    /**
     * @ORM\Column(type="datetime", name="modified_at")
     */
    private ?DateTime $modifiedAt = null;

    /**
     * @ORM\Column(type="integer", name="cancellation", nullable=true)
     */
    private int $cancellation = self::CANCELLATION_NOT;


    /**
     * method suitable for json response
     */
    public function toArray(){
        return [
            'id' => $this->getId(),
            'idUser' => $this->getIdUser(),
            'idTicket' => $this->getIdTicket(),
            'createdAt' => $this->getCreatedAt(),
            'modifiedAt' => $this->getModifiedAt(),
            'state'  => $this->getCancellation() == self::CANCELLATION_YES ? 'Отмена  покупки' : 'Продан',
        ];
    }

}
