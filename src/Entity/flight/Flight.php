<?php
namespace App\Entity\flight;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Entity\flight\FlightRepository")
 * @ORM\Table(name="flight")
 */
class Flight {

    /**
     * @ORM\Column(type="integer", name="id")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private ?int $id = null;

    /**
     * @return int|null
     */
    public function getId():?int{
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId(?int $id):void{
        $this->id = $id;
    }

    /**
     * @return string|null
     */
    public function getName():?string{
        return $this->name;
    }

    /**
     * @param string|null $name
     */
    public function setName(?string $name):void{
        $this->name = $name;
    }

    /**
     * @return DateTime|null
     */
    public function getDepartureAt():?DateTime{
        return $this->departureAt;
    }

    /**
     * @param DateTime|null $departureAt
     */
    public function setDepartureAt(?DateTime $departureAt):void{
        $this->departureAt = $departureAt;
    }

    /**
     * @return int|null
     */
    public function getCountTicket():?int{
        return $this->countTicket;
    }

    /**
     * @param int|null $countTicket
     */
    public function setCountTicket(?int $countTicket):void{
        $this->countTicket = $countTicket;
    }

    /**
     * @ORM\Column(type="string", name="name")
     */
    private ?string $name = null;

    /**
     * @ORM\Column(type="datetime", name="departure_at")
     */
    private ?DateTime $departureAt = null;

    /**
     * @ORM\Column(type="integer", name="count_ticket", nullable=true)
     */
    private ?int $countTicket = null;

	
	/**
	 * method suitable for json response
	 */
	public function toArray(){
		return [
			'id' => $this->getId(),
			'name' => $this->getName(),
			'departureAt' => $this->getDepartureAt(),
			'count_ticket'  => $this->getCountTicket(),
		];
	}

}
