<?php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class IndexController extends AbstractBaseController{

    /**
     * @Route("/", name="index")
     */
    public function index(Request $request):Response{
        return new Response('slotegrator-api');
    }
	

}
