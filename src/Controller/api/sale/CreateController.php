<?php
namespace App\Controller\api\sale;

use App\Controller\AbstractJwtProtectedController;
use App\Entity\reservation\Reservation;
use App\Entity\ticket\Ticket;
use App\Form\sale\Create;
use App\Form\sale\CreateValidator;
use App\Model\RequestErrors;
use App\Service\ParametersApp;
use App\Service\reservation\ReservationManager;
use App\Service\sale\SaleManager;
use App\Service\ticket\TicketManager;
use App\Service\user\UserManager;
use App\Service\utils\AviaSalesUtils;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class CreateController extends AbstractJwtProtectedController{
    private CreateValidator $createValidator;
    private SaleManager $saleManager;
    private TicketManager $ticketManager;
    private ReservationManager $reservationManager;

    public function __construct(AviaSalesUtils $utils, ParametersApp $parametersApp, UserManager $userManager,
                                CreateValidator $createValidator, SaleManager $saleManager,
                                TicketManager $ticketManager, ReservationManager $reservationManager){
        parent::__construct($utils, $parametersApp, $userManager);
        $this->createValidator = $createValidator;
        $this->saleManager = $saleManager;
        $this->ticketManager = $ticketManager;
        $this->reservationManager = $reservationManager;
    }

    /**
     * @Route("/sale/create", name="createSale", methods={"POST"})
     */
    public function create(Request $request):Response{
		$requestParams = json_decode($request->getContent(), true);
		$createForm = new Create();
		$createForm->loadFromRequest($requestParams);
	
		$requestErrors = $this->createValidator->validate($createForm);
		if ((count($requestErrors->getErrors()) > 0)) {
			return $this->response400($requestErrors);
		}

        $ticket = $this->ticketManager->ticketRepository->findOneBy(['id' => $requestParams['idTicket']]);
        if ($ticket === null) {
            $requestErrors = new RequestErrors();
            $requestErrors->addError('Билет не найден');
            return $this->response404($requestErrors);
        }

        if ($ticket->getState() == Ticket::STATE_BOUGHT) {
            $requestErrors = new RequestErrors();
            $requestErrors->addError('Билет уже продан');
            return $this->response403($requestErrors);
        }

        if ($ticket->getState() == Ticket::STATE_RESERVED) {
            $reservation = $this->reservationManager->reservationRepository
                ->findOneBy(['idUser' => $this->loggedUser->getId(), 'idTicket' => $ticket->getId(),
                             'cancellation' => Reservation::CANCELLATION_NOT]);
            if ($reservation === null) {
                $requestErrors = new RequestErrors();
                $requestErrors->addError('Билет уже зарезервирован не Вами');
                return $this->response403($requestErrors);
            }
        }
		$sale = $this->saleManager->createRowFromRequestParams($requestParams, $this->loggedUser);
		$this->saleManager->saveSimple($sale);
		$ticket->setState(Ticket::STATE_BOUGHT);
		$this->ticketManager->saveSimple($ticket);

		$freeTicketsOnFlight = $this->ticketManager->ticketRepository
            ->findBy(['idFlight' => $ticket->getIdFlight(), 'state' => [Ticket::STATE_DEFAULT, Ticket::STATE_RESERVED]]);
		// TODO некая логика связанная с тем что все билеты проданы

        return  $this->response200(['Продажа' =>  $sale->toArray()]);
    }
    
}
