<?php
namespace App\Controller\api\sale;

use App\Controller\AbstractJwtProtectedController;
use App\Entity\sale\Sale;
use App\Entity\ticket\Ticket;
use App\Form\sale\Cancel;
use App\Form\sale\CancelValidator;
use App\Model\RequestErrors;
use App\Service\ParametersApp;
use App\Service\reservation\ReservationManager;
use App\Service\sale\SaleManager;
use App\Service\ticket\TicketManager;
use App\Service\user\UserManager;
use App\Service\utils\AviaSalesUtils;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use DateTime;

class CancelController extends AbstractJwtProtectedController{
    private CancelValidator $cancelValidator;
    private SaleManager $saleManager;
    private TicketManager $ticketManager;

    public function __construct(AviaSalesUtils $utils, ParametersApp $parametersApp, UserManager $userManager,
                                CancelValidator $cancelValidator, TicketManager $ticketManager, SaleManager $saleManager){
        parent::__construct($utils, $parametersApp, $userManager);
        $this->cancelValidator = $cancelValidator;
        $this->ticketManager = $ticketManager;
        $this->saleManager = $saleManager;
    }

    /**
     * @Route("/sale/cancel", name="cancelSale", methods={"POST"})
     */
    public function cancel(Request $request):Response{
		$requestParams = json_decode($request->getContent(), true);
		$cancelForm = new Cancel();
		$cancelForm->loadFromRequest($requestParams);
	
		$requestErrors = $this->cancelValidator->validate($cancelForm);
		if ((count($requestErrors->getErrors()) > 0)) {
			return $this->response400($requestErrors);
		}

        $sale = $this->saleManager->saleRepository->findOneBy(['id' => $requestParams['id']]);
        if ($sale === null) {
            $requestErrors = new RequestErrors();
            $requestErrors->addError('Продажа не найдена');
            return $this->response404($requestErrors);
        }
		$ticket = $this->ticketManager->ticketRepository->findOneBy(['id' => $sale->getIdTicket()]);
        if ($ticket === null) {
            $requestErrors = new RequestErrors();
            $requestErrors->addError('Билет не найден');
            return $this->response404($requestErrors);
        }

        if ($ticket->getState() != Ticket::STATE_BOUGHT || $sale->getIdUser() != $this->loggedUser->getId()) {
            $requestErrors = new RequestErrors();
            $requestErrors->addError('У Вас нет прав для отмены покупки билета');
            return $this->response403($requestErrors);
        }
	
		$sale->setCancellation(Sale::CANCELLATION_YES);
        $sale->setModifiedAt(new DateTime('now'));
		$this->saleManager->saveSimple($sale);
		$ticket->setState(Ticket::STATE_DEFAULT);
		$this->ticketManager->saveSimple($ticket);

		return  $this->response200(['Продажа' =>  $sale->toArray()]);
    }
    
}
