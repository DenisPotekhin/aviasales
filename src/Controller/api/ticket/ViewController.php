<?php
namespace App\Controller\api\ticket;

use App\Controller\AbstractJwtProtectedController;
use App\Entity\ticket\Ticket;
use App\Model\RequestErrors;
use App\Service\ParametersApp;
use App\Service\ticket\TicketManager;
use App\Service\user\UserManager;
use App\Service\utils\AviaSalesUtils;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ViewController extends AbstractJwtProtectedController{
    private TicketManager $ticketManager;

    public function __construct(AviaSalesUtils $utils, ParametersApp $parametersApp, UserManager $userManager,
                                TicketManager $ticketManager){
        parent::__construct($utils, $parametersApp, $userManager);
        $this->ticketManager = $ticketManager;
    }

    /**
     * @Route("/ticket/{id}", name="ticketView", methods={"GET"})
     */
    public function view(string $id):Response{
        $tickets = $this->ticketManager->ticketRepository
            ->findBy(['idFlight' => (int) $id, 'state' => Ticket::STATE_DEFAULT]);
        $result = [];
        foreach($tickets as $ticket){
            $result[] = $ticket->getId();
        }
        return $this->response200(['Свободные билеты на рейс' => $result]);
    }

}
