<?php
namespace App\Controller\api\user;

use App\Controller\AbstractBaseController;
use App\Entity\user\User;
use App\Form\user\Login;
use App\Form\user\LoginValidator;
use App\Model\RequestErrors;
use App\Service\ParametersApp;
use App\Service\user\UserManager;
use App\Service\utils\AviaSalesUtils;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class LoginController extends AbstractBaseController{
    private LoginValidator $loginValidator;

    public function __construct(AviaSalesUtils $utils, ParametersApp $parametersApp, UserManager $userManager,
                                LoginValidator $loginValidator){
        parent::__construct($utils, $parametersApp, $userManager);
        $this->loginValidator = $loginValidator;
    }

    /**
     * @Route("/user/login", name="login", methods={"POST"})
     */
    public function login(Request $request):Response{
        // header('Access-Control-Allow-Origin: *');

        //--- get data from request, validate them & save the user
        $requestParams = json_decode($request->getContent(), true);
        $loginForm = new Login();
        $loginForm->loadFromRequest($requestParams);
        $requestErrors = $this->loginValidator->validate($loginForm);
        if (count($requestErrors->getErrors()) > 0) {
            return $this->response400($requestErrors);
        }

        /**@var User $user*/
        $user = $this->userManager->userRepository->findOneBy(['login' => $requestParams['login']]);
        if ($user === null) {
            $requestErrors = new RequestErrors();
            $requestErrors->addError('user not found');
            return $this->response404($requestErrors);
        }

        if (!password_verify($requestParams['password'], $user->getPassword()) || !$user->getIsActive()) {
            $requestErrors = new RequestErrors();
            $requestErrors->addError('wrong login/password or user was not activated');
            return $this->response403($requestErrors);
        }


        $jwt = $this->createNewJwtByUser($user);
        return  $this->response200([
            'jwt-token' => $jwt,
        ]);
	}

}
