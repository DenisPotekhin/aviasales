<?php
namespace App\Controller\api\user;

use App\Controller\AbstractJwtProtectedController;
use App\Entity\user\User;
use App\Service\ParametersApp;
use App\Service\user\UserManager;
use App\Service\utils\AviaSalesUtils;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ListingController extends AbstractJwtProtectedController{
    public function __construct(AviaSalesUtils $utils, ParametersApp $parametersApp, UserManager $userManager){
        parent::__construct($utils, $parametersApp, $userManager);
    }

    /**
     * @Route("/users", name="users", methods={"GET"})
     */
    public function users(Request $request):Response{
        $result = [];
        
        // TODO needs to rebuild
		$queryParams = $request->query->all();
		$limit = isset($queryParams['limit']) ? $queryParams['limit'] : 20;
		$offset = isset($queryParams['offset']) ? $queryParams['offset'] : 0;
		//-----------------------
		
        /**@var User[] $users*/
        $users = $this->userManager->userRepository->findBy([], ['surname' => 'ASC'], $limit, $offset);
        foreach ($users as $user) {
            $resultUser = $user->toArray();
            $result[] = $resultUser;
        }
        return  $this->response200(['users' => $result]);
    }

}
