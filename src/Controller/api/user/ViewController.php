<?php
namespace App\Controller\api\user;

use App\Controller\AbstractJwtProtectedController;
use App\Model\RequestErrors;
use App\Service\ParametersApp;
use App\Service\user\UserManager;
use App\Service\utils\AviaSalesUtils;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ViewController extends AbstractJwtProtectedController{
    public function __construct(AviaSalesUtils $utils, ParametersApp $parametersApp, UserManager $userManager){
        parent::__construct($utils, $parametersApp, $userManager);
    }

    /**
     * @Route("/user/{uuid}", name="userView", methods={"GET"})
     */
    public function view(string $uuid):Response{
        $user = $this->userManager->userRepository->findOneBy(['uuid' => $uuid]);
        if($user === null) {
            $requestErrors = new RequestErrors();
            $requestErrors->addError('user not found');
            return $this->response404($requestErrors);
        }
        return $this->response200(['user' => $user->toArray()]);
    }

}
