<?php
namespace App\Controller\api\user;

use App\Controller\AbstractBaseController;
use App\Entity\user\User;
use App\Form\user\Register;
use App\Form\user\RegisterValidator;
use App\Model\RequestErrors;
use App\Service\ParametersApp;
use App\Service\user\EmailTokenManager;
use App\Service\user\UserManager;
use App\Service\utils\AviaSalesUtils;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class RegistrationController extends AbstractBaseController{
    private EmailTokenManager $emailTokenManager;
    private RegisterValidator $registerValidator;

    public function __construct(AviaSalesUtils $utils, ParametersApp $parametersApp, UserManager $userManager,
                                EmailTokenManager $emailTokenManager, RegisterValidator $registerValidator){
        parent::__construct($utils, $parametersApp, $userManager);
        $this->emailTokenManager = $emailTokenManager;
        $this->registerValidator = $registerValidator;

    }

    /**
     * @Route("/user/registration", name="userRegistration", methods={"POST"})
     */
    public function register(Request $request):Response{
        //--- get data from request, validate them
        $requestParams = json_decode($request->getContent(), true);
        $registerForm = new Register();
        $registerForm->loadFromRequest($requestParams);
        $requestErrors = $this->registerValidator->validate($registerForm);
        if (count($requestErrors->getErrors()) > 0) {
            return $this->response400($requestErrors);
        }

        $user = new User();
        $this->userManager->createUserFromRequestParams($user, $requestParams);
        $this->userManager->saveSimple($user);

        $emailToken = $this->emailTokenManager->createNewActivateEmail($user);
        $this->emailTokenManager->sendActivateEmail($emailToken, $user);

        $this->emailTokenManager->saveSimple($emailToken);

        return  $this->response200(['user generated, activate email sent to user']);
    }

    /**
     * @Route("/user/activation/{emailToken}", name="userActivation", methods={"GET"})
     */
    public function activation(Request $request, string $emailToken):Response{
        $emailTokenObject = $this->emailTokenManager->emailTokenRepository->findOneBy(['token' => $emailToken]);
        if ($emailTokenObject === null) {
            $requestErrors = new RequestErrors();
            $requestErrors->addError('wrong token');
            return $this->response403($requestErrors);
        }
        if (($emailTokenObject->getExpiredAt()->getTimestamp() - time()) < 0) {
            $requestErrors = new RequestErrors();
            $requestErrors->addError('token was expired	');
            return $this->response403($requestErrors);
        }

        $user = $this->userManager->userRepository->findOneBy(['id' => $emailTokenObject->getIdUser()]);
        if ($user === null) {
            $requestErrors = new RequestErrors();
            $requestErrors->addError('user not found');
            return $this->response404($requestErrors);
        }

        $this->emailTokenManager->deleteItems([$emailTokenObject]);
        $user->setIsActive(USER::ACTIVE);
        $this->userManager->saveSimple($user);

        $jwt = $this->createNewJwtByUser($user);



        return  $this->response200([
            'jwt' => $jwt,
        ]);
    }

}
