<?php
namespace App\Controller\api\reservation;

use App\Controller\AbstractJwtProtectedController;
use App\Entity\reservation\Reservation;
use App\Entity\ticket\Ticket;
use App\Form\reservation\Cancel;
use App\Form\reservation\CancelValidator;
use App\Model\RequestErrors;
use App\Service\ParametersApp;
use App\Service\reservation\ReservationManager;
use App\Service\ticket\TicketManager;
use App\Service\user\UserManager;
use App\Service\utils\AviaSalesUtils;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use DateTime;

class CancelController extends AbstractJwtProtectedController{
    private CancelValidator $cancelValidator;
    private ReservationManager $reservationManager;
    private TicketManager $ticketManager;

    public function __construct(AviaSalesUtils $utils, ParametersApp $parametersApp, UserManager $userManager,
                                CancelValidator $cancelValidator, ReservationManager $reservationManager,
                                TicketManager $ticketManager){
        parent::__construct($utils, $parametersApp, $userManager);
        $this->cancelValidator = $cancelValidator;
        $this->reservationManager = $reservationManager;
        $this->ticketManager = $ticketManager;
    }

    /**
     * @Route("/reservation/cancel", name="cancelReservation", methods={"POST"})
     */
    public function cancel(Request $request):Response{
		$requestParams = json_decode($request->getContent(), true);
		$cancelForm = new Cancel();
		$cancelForm->loadFromRequest($requestParams);
	
		$requestErrors = $this->cancelValidator->validate($cancelForm);
		if ((count($requestErrors->getErrors()) > 0)) {
			return $this->response400($requestErrors);
		}

        $reservation = $this->reservationManager->reservationRepository->findOneBy(['id' => $requestParams['id']]);
        if ($reservation === null) {
            $requestErrors = new RequestErrors();
            $requestErrors->addError('Бронь не найдена');
            return $this->response404($requestErrors);
        }
		$ticket = $this->ticketManager->ticketRepository->findOneBy(['id' => $reservation->getIdTicket()]);
        if ($ticket === null) {
            $requestErrors = new RequestErrors();
            $requestErrors->addError('Билет не найден');
            return $this->response404($requestErrors);
        }

        if ($ticket->getState() != Ticket::STATE_RESERVED || $reservation->getIdUser() != $this->loggedUser->getId()) {
            $requestErrors = new RequestErrors();
            $requestErrors->addError('У Вас нет прав для отмены брони на этот билет');
            return $this->response403($requestErrors);
        }
	
		$reservation->setCancellation(Reservation::CANCELLATION_YES);
        $reservation->setModifiedAt(new DateTime('now'));
		$this->reservationManager->saveSimple($reservation);
		$ticket->setState(Ticket::STATE_DEFAULT);
		$this->ticketManager->saveSimple($ticket);

		return  $this->response200(['Бронь' =>  $reservation->toArray()]);
    }
    
}
