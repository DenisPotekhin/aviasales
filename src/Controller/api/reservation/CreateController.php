<?php
namespace App\Controller\api\reservation;

use App\Controller\AbstractJwtProtectedController;
use App\Entity\ticket\Ticket;
use App\Form\reservation\Create;
use App\Form\reservation\CreateValidator;
use App\Model\RequestErrors;
use App\Service\ParametersApp;
use App\Service\reservation\ReservationManager;
use App\Service\ticket\TicketManager;
use App\Service\user\UserManager;
use App\Service\utils\AviaSalesUtils;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class CreateController extends AbstractJwtProtectedController{
    private CreateValidator $createValidator;
    private ReservationManager $reservationManager;
    private TicketManager $ticketManager;

    public function __construct(AviaSalesUtils $utils, ParametersApp $parametersApp, UserManager $userManager,
                                CreateValidator $createValidator, ReservationManager $reservationManager,
                                TicketManager $ticketManager){
        parent::__construct($utils, $parametersApp, $userManager);
        $this->createValidator = $createValidator;
        $this->reservationManager = $reservationManager;
        $this->ticketManager = $ticketManager;
    }

    /**
     * @Route("/reservation/create", name="createReservation", methods={"POST"})
     */
    public function create(Request $request):Response{
		$requestParams = json_decode($request->getContent(), true);
		$createForm = new Create();
		$createForm->loadFromRequest($requestParams);
	
		$requestErrors = $this->createValidator->validate($createForm);
		if ((count($requestErrors->getErrors()) > 0)) {
			return $this->response400($requestErrors);
		}

        $ticket = $this->ticketManager->ticketRepository->findOneBy(['id' => $requestParams['idTicket']]);
        if ($ticket === null) {
            $requestErrors = new RequestErrors();
            $requestErrors->addError('Билет не найден');
            return $this->response404($requestErrors);
        }

        if ($ticket->getState() != Ticket::STATE_DEFAULT) {
            $requestErrors = new RequestErrors();
            $message = ($ticket->getState() == Ticket::STATE_RESERVED) ? 'Билет уже зарезервирован' : 'Билет уже продан';
            $requestErrors->addError($message);
            return $this->response403($requestErrors);
        }
	
		$reservation = $this->reservationManager->createRowFromRequestParams($requestParams, $this->loggedUser);
		$this->reservationManager->saveSimple($reservation);
		$ticket->setState(Ticket::STATE_RESERVED);
		$this->ticketManager->saveSimple($ticket);

		return  $this->response200(['Бронь' =>  $reservation->toArray()]);
    }
    
}
