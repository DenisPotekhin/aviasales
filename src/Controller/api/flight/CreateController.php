<?php
namespace App\Controller\api\flight;

use App\Controller\AbstractJwtProtectedController;
use App\Entity\ticket\Ticket;
use App\Form\flight\Create;
use App\Form\flight\CreateValidator;
use App\Service\flight\FlightManager;
use App\Service\ParametersApp;
use App\Service\ticket\TicketManager;
use App\Service\user\UserManager;
use App\Service\utils\AviaSalesUtils;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class CreateController extends AbstractJwtProtectedController{
    private CreateValidator $createValidator;
    private FlightManager $flightManager;
    private TicketManager $ticketManager;

    public function __construct(AviaSalesUtils $utils, ParametersApp $parametersApp, UserManager $userManager,
                                CreateValidator $createValidator, FlightManager $flightManager, TicketManager $ticketManager){
        parent::__construct($utils, $parametersApp, $userManager);
        $this->createValidator = $createValidator;
        $this->flightManager = $flightManager;
        $this->ticketManager = $ticketManager;
    }

    /**
     * @Route("/flight/create", name="createFlight", methods={"POST"})
     */
    public function create(Request $request):Response{
		$requestParams = json_decode($request->getContent(), true);
		$createForm = new Create();
		$createForm->loadFromRequest($requestParams);
	
		$requestErrors = $this->createValidator->validate($createForm);
		if ((count($requestErrors->getErrors()) > 0)) {
			return $this->response400($requestErrors);
		}
	
		$flight = $this->flightManager->createRowFromRequestParams($requestParams);
		$this->flightManager->saveSimple($flight);
        $countTickets = $flight->getCountTicket();
        $idFlight = $flight->getId();
        for($i = 1; $i <= $countTickets; $i++){
            $ticket = new Ticket();
            $ticket->setIdFlight($idFlight);
            $ticket->setNumber($i);
            $this->ticketManager->saveSimple($ticket);
        }
	
		return  $this->response200(['Рейс' =>  $flight->toArray()]);
    }
    
}
