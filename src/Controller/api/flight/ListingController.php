<?php
namespace App\Controller\api\flight;

use App\Controller\AbstractJwtProtectedController;
use App\Service\flight\FlightManager;
use App\Service\ParametersApp;
use App\Service\user\UserManager;
use App\Service\utils\AviaSalesUtils;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ListingController extends AbstractJwtProtectedController{
    private FlightManager $flightManager;

    public function __construct(AviaSalesUtils $utils, ParametersApp $parametersApp,
                                UserManager $userManager, FlightManager $flightManager){
        parent::__construct($utils, $parametersApp, $userManager);
        $this->flightManager = $flightManager;
    }

    /**
     * @Route("/flights", name="flights", methods={"GET"})
     */
    public function flights(Request $request):Response{
        $result = [];
        
		$queryParams = $request->query->all();
		$limit = isset($queryParams['limit']) ? $queryParams['limit'] : 20;
		$offset = isset($queryParams['offset']) ? $queryParams['offset'] : 0;
		//-----------------------
		
        $flights = $this->flightManager->flightRepository->findBy([], ['id' => 'ASC'], $limit, $offset);
        foreach ($flights as $flight) {
            $resultFlight = $flight->toArray();
            $result[] = $resultFlight;
        }
        return  $this->response200(['Рейсы' => $result]);
    }

}
