<?php
namespace App\Controller\api\notification;

use App\Controller\AbstractBaseController;
use App\Entity\notification\Notification;
use App\Entity\sale\Sale;
use App\Entity\ticket\Ticket;
use App\Event\NotificationCompleteSaleEvent;
use App\Event\NotificationFlightCancelEvent;
use App\Form\event\Event;
use App\Form\event\EventValidator;
use App\Model\RequestErrors;
use App\Service\flight\FlightManager;
use App\Service\notification\NotificationManager;
use App\Service\ParametersApp;
use App\Service\sale\SaleManager;
use App\Service\ticket\TicketManager;
use App\Service\user\UserManager;
use App\Service\utils\AviaSalesUtils;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class EventController extends AbstractBaseController{
    private EventValidator $eventValidator;
    private FlightManager $flightManager;
    private TicketManager $ticketManager;
    private SaleManager $saleManager;
    private NotificationManager $notificationManager;
    private EventDispatcherInterface $eventDispatcher;

    public function __construct(AviaSalesUtils $utils, ParametersApp $parametersApp, UserManager $userManager,
                                EventValidator $eventValidator, FlightManager $flightManager,
                                TicketManager $ticketManager, SaleManager $saleManager,
                                NotificationManager $notificationManager, EventDispatcherInterface $eventDispatcher){
        parent::__construct($utils, $parametersApp, $userManager);
        $this->eventValidator = $eventValidator;
        $this->flightManager = $flightManager;
        $this->ticketManager = $ticketManager;
        $this->saleManager = $saleManager;
        $this->notificationManager = $notificationManager;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @Route("/callback/events", name="events", methods={"POST"})
     */
    public function events(Request $request):Response{
		$requestParams = json_decode($request->getContent(), true);

		$eventForm = new Event();
		$eventForm->loadFromRequest($requestParams);

		$requestErrors = $this->eventValidator->validate($eventForm);
		if ((count($requestErrors->getErrors()) > 0)) {
			return $this->response400($requestErrors);
		}

        $notification = $this->notificationManager->createRowFromRequestParams($requestParams);
		if ($notification->getType() == Notification::SALE_COMPLETE){
            $notificationCompleteSaleEvent = new NotificationCompleteSaleEvent($notification);
            $this->eventDispatcher->dispatch($notificationCompleteSaleEvent, NotificationCompleteSaleEvent::NAME);
        } else {
            $notificationFlightCancelEvent = new NotificationFlightCancelEvent($notification);
            $this->eventDispatcher->dispatch($notificationFlightCancelEvent, NotificationFlightCancelEvent::NAME);
        }
        $this->notificationManager->saveSimple($notification);

		return  $this->response200(['event' =>  $requestParams['type']]);
    }
    
}
