<?php
namespace App\Controller;

use App\Controller\api\TokenAuthenticatedController;
use App\Entity\user\User;
use App\Model\JwtFromHeaders;
use App\Service\ParametersApp;
use App\Service\user\UserManager;
use App\Service\utils\AviaSalesUtils;

abstract class AbstractJwtProtectedController extends AbstractBaseController implements TokenAuthenticatedController {
    public ?User $loggedUser = null;
    public ?JwtFromHeaders $jwtFromHeaders = null;

    protected function __construct(AviaSalesUtils $utils, ParametersApp $parametersApp, UserManager $userManager){
        parent::__construct($utils, $parametersApp, $userManager);
        $this->jwtFromHeaders = new JwtFromHeaders();
    }

    /**
     * @param $routename
     * @return mixed
     */
    protected function routeToControllerName($routename) {
        $routes = $this->get('router')->getRouteCollection();
        return $routes->get($routename)->getDefaults()['_controller'];
    }
    
}
