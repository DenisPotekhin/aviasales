<?php
namespace App\Controller;

use App\Model\RequestErrors;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ErrorController extends AbstractController
{
    /**
     * 404 - page not found
     * @Route("/error-404", name="CError404")
     */
    public function notFoundAction(){
        $response = new Response();
        $jsonString = json_encode(['page not found']);
        $response->setContent($jsonString);
        $response->setStatusCode(404);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

}
