<?php
namespace App\Controller;

use App\Entity\user\User;
use App\Model\JwtPayload;
use App\Model\RequestErrors;
use App\Service\ParametersApp;
use App\Service\user\UserManager;
use App\Service\utils\AviaSalesUtils;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

abstract class AbstractBaseController extends AbstractController {
    public AviaSalesUtils $utils;
    public ParametersApp $parametersApp;
    public UserManager $userManager;

    protected function __construct(AviaSalesUtils $utils, ParametersApp $parametersApp, UserManager $userManager){
        $this->utils = $utils;
        $this->parametersApp = $parametersApp;
        $this->userManager = $userManager;
    }

    /**
     * @param $routename
     * @return mixed
     */
    protected function routeToControllerName($routename) {
        $routes = $this->get('router')->getRouteCollection();
        return $routes->get($routename)->getDefaults()['_controller'];
    }

    /**
     *
     */
    protected function createNewJwtByUser(User $user):string {
        $jwtPayload = new JwtPayload();
        $jwtPayload->uuidUser = $user->getUuid();
        $jwtPayload->issuedAt = time();
        $jwtPayload->expiration = strtotime("+60 minute");
        $jwtPayload->userName = $user->getName() . ' ' . $user->getSurname();

        return $this->utils->jwt->encode($jwtPayload->toArray(), $this->parametersApp->secretKey);
    }

    /**
     * 200 response
     */
    protected function response200(array $data, string $contentType = 'application/json'):Response{
        $jsonString = json_encode($data);
        return $this->returnResponse(200, $contentType, $jsonString);
    }

    /**
     * 400 response
     */
    public function response400(RequestErrors $requestErrors, string $contentType = 'application/json'):Response{
        $json = json_encode($requestErrors->toArray());
        return $this->returnResponse(400, $contentType, $json);
    }

    /**
     * 403 response
     */
    public function response403(RequestErrors $requestErrors, string $contentType = 'application/json'):Response{
        $json = json_encode($requestErrors->toArray());
        return $this->returnResponse(403, $contentType, $json);
    }

    /**
     * 404 response
     */
    public function response404(RequestErrors $requestErrors, string $contentType = 'application/json'):Response{
        $json = json_encode($requestErrors->toArray());
        return $this->returnResponse(404, $contentType, $json);
    }

    /**
     * return Response by parameters
     */
    public function returnResponse(int $code, string $contentType, string $data):Response {
        $response = new Response();
        $response->setContent($data);
        $response->setStatusCode($code);
        $response->headers->set('Content-Type', $contentType);
        return $response;
    }
}
