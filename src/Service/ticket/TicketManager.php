<?php
namespace App\Service\ticket;

use App\Entity\flight\FlightRepository;
use App\Entity\history\Flight;

use App\Entity\ticket\Ticket;
use App\Entity\ticket\TicketRepository;
use App\Service\AbstractBaseManager;
use App\Service\utils\AviaSalesUtils;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;

class TicketManager extends AbstractBaseManager {

    public TicketRepository $ticketRepository;

    /**
     * AbstractBaseManager constructor.
     */
    public function __construct(EntityManagerInterface $entityManager, AviaSalesUtils $utils){
        parent::__construct($entityManager, $utils);
        $this->ticketRepository = $this->em->getRepository(Ticket::class);
    }

}
