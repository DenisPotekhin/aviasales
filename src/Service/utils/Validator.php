<?php
namespace App\Service\utils;

class Validator {

    public function __construct(){
    }

    //--- validation methods for request parameters ---
    public function paramIsEmptyString(array $requestParams, string $fieldName):bool{
        if(isset($requestParams[$fieldName])){
            return self::isEmptyString($requestParams[$fieldName]);
        }
        return true;
    }

    public function paramHasStringMinLength(array $requestParams, string $fieldName, int $length):bool{
        if(isset($requestParams[$fieldName])){
            return self::hasStringMinLength($requestParams[$fieldName], $length);
        }
        return false;
    }

    public function paramIsEmail(array $requestParams, string $fieldName):bool{
        if(isset($requestParams[$fieldName])){
            return self::isEmail($requestParams[$fieldName]);
        }
        return false;
    }

    public function paramIsInArray(array $requestParams, string $fieldName, array $array):bool{
        if(isset($requestParams[$fieldName])){
            return self::isInArray($requestParams[$fieldName], $array);
        }
        return false;
    }

    //--- simple validation methods ---
    public function isEmptyString(string $value):bool {
        return strlen($value) === 0;
    }

    public function hasStringMinLength(string $value, int $length):bool{
        return strlen($value) > $length;
    }

    public function isEmail(string $value):bool {
        return filter_var($value, FILTER_VALIDATE_EMAIL);
    }

    public function isInArray(string $value, array $array):bool{
        return in_array($value, $array);
    }

}
