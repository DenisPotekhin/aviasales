<?php
namespace App\Service\utils;

class UuidUtil {

    public function __construct(){
    }

    /**
     * generate version 4 of Uuid
     */
    function createUuid4(){
        $ret = null;
        try{
            $data = random_bytes(16);
            $data[6] = chr(ord($data[6]) & 0x0f | 0x40);
            $data[8] = chr(ord($data[8]) & 0x3f | 0x80);
            $ret = vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
        }catch(\Exception $e){

        }
        return $ret;
    }

}
