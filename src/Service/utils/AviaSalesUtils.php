<?php
namespace App\Service\utils;

class AviaSalesUtils
{
    public DateTimeUtil $dateTime;
    public JwtUtil $jwt;
    public JsonUtil $json;
    public UuidUtil $uuid;
    public Validator $validator;

    public function __construct(DateTimeUtil $dateTimeUtil, JwtUtil $jwtUtil, JsonUtil $jsonUtil,
                                UuidUtil $uuidUtil, Validator $validtor){

        $this->dateTime = $dateTimeUtil;
        $this->jwt = $jwtUtil;
        $this->json = $jsonUtil;
        $this->uuid = $uuidUtil;
        $this->validator = $validtor;
    }
}
