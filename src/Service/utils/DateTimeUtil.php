<?php
namespace App\Service\utils;

use DateTime;

class DateTimeUtil {
    /**
     * DateTimeUtil constructor.
     */
    public function __construct(){
    }


    /**
     * creates new DateTime object modified by modifyString and timePart
     * @throws \Exception
     */
    public function newDateTimeFromNow(string $modifyString, ?string $timePart = null):DateTime{
        $dateTime = new DateTime();
        $dateTime->modify($modifyString);
        if($timePart !== null){
            $timeParts = explode(':', $timePart);
            $hours = isset($timeParts[0]) ? $timeParts[0] : 0;
            $minutes = isset($timeParts[1]) ? $timeParts[1] : 0;
            $seconds = isset($timeParts[2]) ? $timeParts[2] : 0;
            $dateTime->setTime($hours, $minutes, $seconds);
        }
        return $dateTime;
    }

}
