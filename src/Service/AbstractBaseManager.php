<?php
namespace App\Service;

use App\Service\utils\AviaSalesUtils;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

abstract class AbstractBaseManager{
    protected AviaSalesUtils $utils;
    protected EntityManagerInterface $em;

    /**
     * AbstractBaseManager constructor.
     */
    public function __construct(EntityManagerInterface $entityManager, AviaSalesUtils $utils){
        $this->em = $entityManager;
        $this->utils = $utils;
    }

    /**
     * remove items from DB
     */
    public function deleteItems($items = []){
        foreach($items as $item){
            $this->em->remove($item);
        }
        $this->em->flush();
    }

    /**
     * save simple object into db
     */
    public function saveSimple($item):bool{
        try{
            $this->em->persist($item);
            $this->em->flush();
            return true;
        }catch(OptimisticLockException $e){
            return false;
        }catch(ORMException $e){
            return false;
        }
    }

}
