<?php
namespace App\Service\user;

use App\Entity\user\User;
use App\Entity\user\UserRepository;
use App\Model\RequestErrors;
use App\Service\AbstractBaseManager;
use App\Service\utils\AviaSalesUtils;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;

class UserManager extends AbstractBaseManager {

    public UserRepository $userRepository;

    /**
     * AbstractBaseManager constructor.
     */
    public function __construct(EntityManagerInterface $entityManager, AviaSalesUtils $utils){
        parent::__construct($entityManager, $utils);
        $this->userRepository = $this->em->getRepository(User::class);
    }


    
    public function createUserFromRequestParams(User $user, array $requestParams):User {
		$user->setIsActive(USER::NOT_ACTIVE);
		$user->setUuid(uniqid());
		
		$user->setEmail($requestParams['email']);
		$user->setName($requestParams['name']);
		$user->setLogin($requestParams['login']);
		$user->setPassword(password_hash($requestParams['password'], PASSWORD_BCRYPT));
		$user->setSurname($requestParams['surname']);
		return $user;
	}


}
