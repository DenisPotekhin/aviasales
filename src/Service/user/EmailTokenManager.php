<?php
namespace App\Service\user;

use App\Entity\user\EmailToken;
use App\Entity\user\EmailTokenRepository;
use App\Entity\user\User;
use App\Service\AbstractBaseManager;
use App\Service\utils\AviaSalesUtils;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;
use Twig\Environment;


class EmailTokenManager extends AbstractBaseManager {

    private MailerInterface $mailer;
	public EmailTokenRepository $emailTokenRepository;
	public Environment $twig;
	protected RouterInterface $router;
    

    /**
     * AbstractBaseManager constructor.
     */
    public function __construct(EntityManagerInterface $entityManager, AviaSalesUtils $utils,
                                MailerInterface $mailer, Environment $twig, RouterInterface $router) {
        parent::__construct($entityManager, $utils);
        $this->emailTokenRepository = $this->em->getRepository(EmailToken::class);
        $this->mailer = $mailer;
		$this->twig = $twig;
		$this->router = $router;
    
    }
    
    public function createNewActivateEmail(User $user):EmailToken {
		$emailToken = new EmailToken();
		$emailToken->setType(EmailToken::TYPE_ACCOUNT_ACTIVATION);
		$emailToken->setIdUser($user->getId());
		return $emailToken;
	}


    public function sendActivateEmail(EmailToken $emailToken, User $user){
	
		$activationUrl = $this->router->generate('userActivation', ['emailToken' => $emailToken->getToken()],
            UrlGeneratorInterface::ABSOLUTE_URL);
		
		$email = (new TemplatedEmail())
			->from('no-reply@aviasales.com')
			->to($user->getEmail())
			->subject('Link for activate registration')
		
			// path of the Twig template to render
			->htmlTemplate('emailTemplates/userActivation.html.twig')
		
			// pass variables (name => value) to the template
			->context([
						  'activationUrl' => $activationUrl,
						  'userFullName' => $user->getName() . ' ' . $user->getSurname(),
						  'userLogin' => $user->getLogin()
					  ]);


		try {
			$this->mailer->send($email);
		} catch (TransportExceptionInterface $e) {
		}
	}
	

}
