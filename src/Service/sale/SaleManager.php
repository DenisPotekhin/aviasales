<?php
namespace App\Service\sale;

use App\Entity\sale\Sale;
use App\Entity\sale\SaleRepository;
use App\Entity\user\User;
use App\Service\AbstractBaseManager;
use App\Service\utils\AviaSalesUtils;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;

class SaleManager extends AbstractBaseManager {

    public SaleRepository $saleRepository;

    /**
     * AbstractBaseManager constructor.
     */
    public function __construct(EntityManagerInterface $entityManager, AviaSalesUtils $utils){
        parent::__construct($entityManager, $utils);
        $this->saleRepository = $this->em->getRepository(Sale::class);
    }
    
    public function createRowFromRequestParams(array $requestParams, User $user):Sale {
		$sale = new Sale();
		$sale->setIdTicket($requestParams['idTicket']);
		$createdAt = new DateTime('now');
		$sale->setCreatedAt($createdAt);
		$sale->setIdUser($user->getId());
		return $sale;
	}


    
}
