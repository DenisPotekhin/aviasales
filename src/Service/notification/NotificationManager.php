<?php
namespace App\Service\notification;

use App\Entity\flight\Flight;
use App\Entity\flight\FlightRepository;

use App\Entity\notification\Notification;
use App\Entity\notification\NotificationRepository;
use App\Service\AbstractBaseManager;
use App\Service\utils\AviaSalesUtils;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;

class NotificationManager extends AbstractBaseManager {

    public NotificationRepository $notificationRepository;

    /**
     * AbstractBaseManager constructor.
     */
    public function __construct(EntityManagerInterface $entityManager, AviaSalesUtils $utils){
        parent::__construct($entityManager, $utils);
        $this->notificationRepository = $this->em->getRepository(Notification::class);
    }
    
    public function createRowFromRequestParams(array $requestParams):Notification {
		$notification = new Notification();
        $notification->setIdFlight($requestParams['idFlight']);
        $triggeredAt = new DateTime($requestParams['triggeredAt']);
        $notification->setTriggeredAt($triggeredAt);
        $notification->setType($requestParams['type']);
		return $notification;
	}


    
}
