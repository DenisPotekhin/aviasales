<?php
namespace App\Service;


class ParametersApp {
    public bool $isProd;
    public string $pathRoot;
    public string $secretKey;
    public int $limit;
    public int $offset;

    /**
     * Parameters for application
     */
    public function __construct(bool $isProd, string $pathRoot, string $secretKey,
                                int $limit, int $offset){
        $this->isProd = $isProd;
        $this->pathRoot = $pathRoot;
        $this->secretKey = $secretKey;
        $this->limit = $limit;
        $this->offset = $offset;
    }

}
