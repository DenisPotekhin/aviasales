<?php
namespace App\Service\reservation;

use App\Entity\flight\Flight;
use App\Entity\flight\FlightRepository;

use App\Entity\reservation\Reservation;
use App\Entity\reservation\ReservationRepository;
use App\Entity\user\User;
use App\Service\AbstractBaseManager;
use App\Service\utils\AviaSalesUtils;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;

class ReservationManager extends AbstractBaseManager {

    public ReservationRepository $reservationRepository;

    /**
     * AbstractBaseManager constructor.
     */
    public function __construct(EntityManagerInterface $entityManager, AviaSalesUtils $utils){
        parent::__construct($entityManager, $utils);
        $this->reservationRepository = $this->em->getRepository(Reservation::class);
    }
    
    public function createRowFromRequestParams(array $requestParams, User $user):Reservation {
		$reservation = new Reservation();
		$reservation->setIdTicket($requestParams['idTicket']);
		$createdAt = new DateTime('now');
		$reservation->setCreatedAt($createdAt);
		$reservation->setIdUser($user->getId());
		return $reservation;
	}


    
}
