<?php
namespace App\Service\flight;

use App\Entity\flight\Flight;
use App\Entity\flight\FlightRepository;

use App\Service\AbstractBaseManager;
use App\Service\utils\AviaSalesUtils;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;

class FlightManager extends AbstractBaseManager {

    public FlightRepository $flightRepository;

    /**
     * AbstractBaseManager constructor.
     */
    public function __construct(EntityManagerInterface $entityManager, AviaSalesUtils $utils){
        parent::__construct($entityManager, $utils);
        $this->flightRepository = $this->em->getRepository(Flight::class);
    }
    
    public function createRowFromRequestParams(array $requestParams):Flight {
		$flight = new Flight();
		$flight->setName($requestParams['name']);
		$departureAt = new DateTime($requestParams['departure']);
		$flight->setDepartureAt($departureAt);
		$flight->setCountTicket($requestParams['count']);
		return $flight;
	}


    
}
