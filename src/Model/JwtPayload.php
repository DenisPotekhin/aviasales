<?php

namespace App\Model;

use stdClass;

class JwtPayload
{
    public ?string $issuer = 'sentia';
    public ?int $expiration = null; // [now+15 minutes] current dateTime MUST be before this value (timestamp)
    public ?int $issuedAt = null; // the time at which the JWT was issued (timestamp)
    public ?string $uuidUser = null;
    public ?string $userName = null;

    /**
     * convert $this object into array suitable for jwt encryption
     */
    public function toArray():array{
        return [
            'iss' => $this->issuer,
            'exp' => $this->expiration,
            'iat' => $this->issuedAt,
            'uuidUser' => $this->uuidUser,
            'userName' => $this->userName
        ];
    }

    public function isExpired(){
        return ($this->expiration !== null && $this->expiration - time() < 0);
    }

    public function fromArray(array $arr):void{
        $this->issuer = isset($arr['iss']) ? $arr['iss'] : null;
        $this->expiration = isset($arr['exp']) ? $arr['exp'] : null;
        $this->issuedAt = isset($arr['iat']) ? $arr['iat'] : null;
        $this->uuidUser = isset($arr['uuidUser']) ? $arr['uuidUser'] : null;
        $this->userName = isset($arr['userName']) ? $arr['userName'] : null;
    }

    public function fromStdClass(stdClass $class):void{
        $this->issuer = isset($class->iss) ? $class->iss : null;
        $this->expiration = isset($class->exp) ? $class->exp : null;
        $this->issuedAt = isset($class->iat) ? $class->iat : null;
        $this->uuidUser = isset($class->uuidUser) ? $class->uuidUser : null;
        $this->userName = isset($class->userName) ? $class->userName : null;
    }
}
