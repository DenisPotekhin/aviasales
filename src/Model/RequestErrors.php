<?php
namespace App\Model;

class RequestErrors
{
    private array $errors = []; // errors not related to any form field
    private array $missingFormFields = []; // missing form fields
    private array $formFieldsErrors = [];

    public function getErrors():array{
        return $this->errors;
    }

    public function getMissingFormFields():array{
        return $this->missingFormFields;
    }

    public function getFormFieldsErrors():array{
        return $this->formFieldsErrors;
    }

    public function addError(string $errorMessage):void{
        $this->errors[] = $errorMessage;
    }

    public function addMissingFormField(string $fieldName):void{
        if(!in_array($fieldName, $this->missingFormFields)){
            $this->missingFormFields[] = $fieldName;
        }
    }

    public function addFormFieldError(string $fieldName, string $errorMessage):void{
        if(!isset($this->formFieldsErrors[$fieldName])){
            $this->formFieldsErrors[$fieldName] = [];
        }
        $this->formFieldsErrors[$fieldName][] = $errorMessage;
    }

    /**
     * convert $this object into array
     */
    public function toArray():array{
        $ret = [];
        $ret['errors'] = $this->errors;

        if(count($this->missingFormFields) > 0){
            $ret['missing-form-fields'] = $this->missingFormFields;
        }

        if(count($this->formFieldsErrors) > 0){
            $ret['form-fields-errors'] = $this->formFieldsErrors;
        }

        return $ret;
    }
}
