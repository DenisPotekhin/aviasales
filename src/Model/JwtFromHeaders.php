<?php
namespace App\Model;

class JwtFromHeaders
{
    public ?string $jwt = null;
    public ?JwtPayload $jwtPayload = null;
    public ?string $error = null;
    
    public function __construct() {
		$this->jwtPayload = new JwtPayload();
    }
}
