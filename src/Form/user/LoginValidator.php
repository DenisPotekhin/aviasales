<?php

namespace App\Form\user;

use App\Form\AbstractValidator;
use App\Service\user\UserManager;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class LoginValidator extends AbstractValidator {
	private ValidatorInterface $validator;
	private UserManager $userManager;
	
	public function __construct(ValidatorInterface $validator, UserManager $userManager) {
		parent::__construct($validator);
		$this->validator = $validator;
		$this->userManager = $userManager;
	}
	
	public function validate(Login $login, ?string $uuidUserToSkip = null){
		$errors = $this->validator->validate($login);
		return $this->getRequestErrors($errors);
		
	}
}
