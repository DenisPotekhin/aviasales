<?php

namespace App\Form\user;

use App\Form\AbstractValidator;
use App\Service\user\UserManager;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class RegisterValidator extends AbstractValidator {
	private ValidatorInterface $validator;
	private UserManager $userManager;
	
	public function __construct(ValidatorInterface $validator, UserManager $userManager) {
		parent::__construct($validator);
		$this->validator = $validator;
		$this->userManager = $userManager;
	}
	
	
	public function validate(Register $register, ?string $uuidUserToSkip = null){
		$errors = $this->validator->validate($register);
		$requestErrors = $this->getRequestErrors($errors);
		
		$user = $this->userManager->userRepository->findOneBy(['email' => $register->email]);
		if ($user){
			$requestErrors->addError('Email is used already');
			$requestErrors->addFormFieldError('email', 'Email is used already');
		}
		$user = $this->userManager->userRepository->findOneBy(['login' => $register->login]);
		if($user){
			$requestErrors->addError('Login is used already');
			$requestErrors->addFormFieldError('login', 'Login is used already');
		}
		
		return $requestErrors;
	}
}
