<?php

namespace App\Form\user;

use App\Entity\user\User;
use Symfony\Component\Validator\Constraints as Assert;


class Login {
	
	/**
	 * @Assert\NotBlank
	 * @Assert\Length(min = 6, max = 255)
	 */
	public ?string $login = null;
	/**
	 * @Assert\NotBlank
	 * @Assert\Length(min = 6, max = 255)
	 */
	public ?string $password = null;
	
	public function loadFromRequest($requestParams){
		
		$this->login = isset($requestParams['login']) ? $requestParams['login'] : null;
		$this->password = isset($requestParams['password']) ? $requestParams['password'] : null;
		
	}
	
}
