<?php

namespace App\Form\user;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class Register {
	/**
	 * @Assert\NotBlank
	 * @Assert\Length(min = 6, max = 255)
	 */
	public ?string $login = null;
	/**
	 * @Assert\NotBlank
	 * @Assert\Email(message = "The email {{ value }} is not a valid email.")
	 */
	public ?string $email = null;
	/**
	 * @Assert\NotBlank
	 * @Assert\Length(min = 1, max = 255)
	 */
	public ?string $name = null;
	/**
	 * @Assert\NotBlank(message = "Name can not be empty")
	 * @Assert\Length(min = 1, max = 255)
	 */
	public ?string $surname = null;
	/**
	 * @Assert\NotBlank
	 * @Assert\Length(min = 6, max = 255)
	 */
	public ?string $password = null;
	
	public function loadFromRequest($requestParams){
		
		$this->name = isset($requestParams['name']) ? $requestParams['name'] : null;
		$this->surname = isset($requestParams['surname']) ? $requestParams['surname'] : null;
		$this->email = isset($requestParams['email']) ? $requestParams['email'] : null;
		$this->login = isset($requestParams['login']) ? $requestParams['login'] : null;
		$this->password = isset($requestParams['password']) ? $requestParams['password'] : null;
		
	}
	
}
