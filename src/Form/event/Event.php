<?php

namespace App\Form\event;

use Symfony\Component\Validator\Constraints as Assert;

class Event {
    /**
     * @Assert\Positive
     */
    public ?string $idFlight = null;

    /**
     * @Assert\Positive
     */
    public ?string $type = null;

    /**
     * @Assert\DateTime
     * @var string A "Y-m-d H:i:s" formatted value
     */
    public ?string $triggeredAt = null;
	
	public function loadFromRequest($requestParams){
		
		$this->idFlight = isset($requestParams['idFlight']) ? $requestParams['idFlight'] : null;
		$this->type = isset($requestParams['type']) ? $requestParams['type'] : null;
        $this->triggeredAt = isset($requestParams['triggeredAt']) ? $requestParams['triggeredAt'] : null;
	}
	
}
