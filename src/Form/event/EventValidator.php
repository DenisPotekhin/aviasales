<?php

namespace App\Form\event;

use App\Form\AbstractValidator;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class EventValidator extends AbstractValidator {
	private ValidatorInterface $validator;
	
	public function __construct(ValidatorInterface $validator) {
		parent::__construct($validator);
		$this->validator = $validator;
	}
	
	
	public function validate(Event $event){
		$errors = $this->validator->validate($event);
		return $this->getRequestErrors($errors);
	}
}
