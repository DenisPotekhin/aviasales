<?php

namespace App\Form\reservation;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class Create {

    /**
     * @Assert\Positive
     */
    public ?string $idTicket = null;
	
	public function loadFromRequest($requestParams){
		
		$this->idTicket = isset($requestParams['idTicket']) ? $requestParams['idTicket'] : null;
	}
	
}
