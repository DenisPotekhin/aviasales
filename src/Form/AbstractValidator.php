<?php

namespace App\Form;

use App\Model\RequestErrors;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class AbstractValidator {
	private ValidatorInterface $validator;
	
	public function __construct(ValidatorInterface $validator) {
		$this->validator = $validator;
	}
	

	public function getRequestErrors($errors):?RequestErrors{
		$requestErrors = new RequestErrors();
		
		if (count($errors) > 0) {
			
			/**@var ConstraintViolation[] $errors*/
			foreach($errors as $error){
				$requestErrors->addError($error->getMessage());
				
				$requestErrors->addFormFieldError($error->getPropertyPath(), $error->getMessage());
				
				if($error->getConstraint() instanceof Assert\NotBlank){
					$requestErrors->addMissingFormField($error->getPropertyPath());
				}
			}
		}
		return $requestErrors;
	}
}
