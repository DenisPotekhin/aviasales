<?php

namespace App\Form\flight;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class Create {
	/**
	 * @Assert\NotBlank
	 * @Assert\Length(min = 1, max = 45)
	 */
	public ?string $name = null;
    /**
     * @Assert\Range(
     *      min = 0,
     *      max = 150,
     * )
     */
    public ?string $countTicket = null;

    /**
     * @Assert\DateTime
     * @var string A "Y-m-d H:i:s" formatted value
     */
    public ?string $departureAt = null;
	
	public function loadFromRequest($requestParams){
		
		$this->name = isset($requestParams['name']) ? $requestParams['name'] : null;
		$this->countTicket = isset($requestParams['count']) ? $requestParams['count'] : null;
        $this->departureAt = isset($requestParams['departure']) ? $requestParams['departure'] : null;
	}
	
}
