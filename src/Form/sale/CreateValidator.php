<?php

namespace App\Form\sale;

use App\Form\AbstractValidator;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class CreateValidator extends AbstractValidator {
	private ValidatorInterface $validator;
	
	public function __construct(ValidatorInterface $validator) {
		parent::__construct($validator);
		$this->validator = $validator;
	}
	
	
	public function validate(Create $create){
		$errors = $this->validator->validate($create);
		return $this->getRequestErrors($errors);
	}
}
