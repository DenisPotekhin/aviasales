<?php

namespace App\Form\sale;

use App\Form\AbstractValidator;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class CancelValidator extends AbstractValidator {
	private ValidatorInterface $validator;
	
	public function __construct(ValidatorInterface $validator) {
		parent::__construct($validator);
		$this->validator = $validator;
	}
	
	
	public function validate(Cancel $cancel){
		$errors = $this->validator->validate($cancel);
		return $this->getRequestErrors($errors);
	}
}
