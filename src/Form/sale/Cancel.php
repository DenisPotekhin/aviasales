<?php

namespace App\Form\sale;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class Cancel {

    /**
     * @Assert\Positive
     */
    public ?string $id = null;
	
	public function loadFromRequest($requestParams){
		
		$this->id = isset($requestParams['id']) ? $requestParams['id'] : null;
	}
	
}
