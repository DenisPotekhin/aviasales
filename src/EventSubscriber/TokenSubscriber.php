<?php
namespace App\EventSubscriber;

use App\Controller\api\TokenAuthenticatedController;
use App\Exceptions\AuthException;
use App\Exceptions\UserNotFoundException;
use App\Model\RequestErrors;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class TokenSubscriber implements EventSubscriberInterface
{
    /**
     * @throws AuthException
     * @throws UserNotFoundException
     */
    public function onKernelController(ControllerEvent $event){
        $controller = $event->getController();
        if (is_array($controller)) {
            $controller = $controller[0];
        }

        if ($controller instanceof TokenAuthenticatedController) {
            $authorizationHeader = $event->getRequest()->headers->get('Authorization');

            if ($authorizationHeader) {
				try {
					$controller->jwtFromHeaders->jwt = substr($authorizationHeader, 7);
					$jwtDecoded = $controller->utils->jwt->decode($controller->jwtFromHeaders->jwt, $controller->parametersApp->secretKey);
					$controller->jwtFromHeaders->jwtPayload->fromStdClass($jwtDecoded);
				} catch (\UnexpectedValueException $exception) {
					throw new AuthException('incorrect jwt-token');
				}
                

                if ($controller->jwtFromHeaders->jwtPayload->isExpired()) {
                    throw new AuthException('token was expired');
                }

                if(!$jwtDecoded) {
                    // $controller->jwtFromHeaders->error = 'incorrect jwt-token';
                    throw new AuthException('incorrect jwt-token');
                }
            }else{
                throw new AuthException('unauthorized');
            }

            if($controller->jwtFromHeaders->error == null){
                $userUuid = $controller->jwtFromHeaders->jwtPayload->uuidUser;
                $controller->loggedUser = $controller->userManager->userRepository->findOneBy(['uuid' => $userUuid]);
                if ($controller->loggedUser === null) {
                    throw new UserNotFoundException('user not found');
                }
            }

        }


        
    }

    public function onKernelException(ExceptionEvent $event){
        $exception = $event->getThrowable();
        $requestErrors = new RequestErrors();
        $requestErrors->addError($exception->getMessage());

        $jsonArr = $requestErrors->toArray();
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent(json_encode($jsonArr));

        if($exception instanceof AuthException){
            $response->setStatusCode(403);
            $event->setResponse($response);
        }

        if($exception instanceof UserNotFoundException) {
            $response->setStatusCode(400);
            $event->setResponse($response);
        }
    }

    public static function getSubscribedEvents(){
        return [
            KernelEvents::CONTROLLER => 'onKernelController',
            KernelEvents::EXCEPTION => 'onKernelException',
        ];
    }
}
