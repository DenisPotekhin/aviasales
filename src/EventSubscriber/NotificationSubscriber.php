<?php
namespace App\EventSubscriber;

use App\Entity\notification\Notification;
use App\Entity\sale\Sale;
use App\Entity\ticket\Ticket;
use App\Event\NotificationCompleteSaleEvent;
use App\Event\NotificationFlightCancelEvent;
use App\Model\RequestErrors;
use App\Service\flight\FlightManager;
use App\Service\sale\SaleManager;
use App\Service\ticket\TicketManager;
use App\Service\user\UserManager;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;

class NotificationSubscriber implements EventSubscriberInterface
{
    private MailerInterface $mailer;
    private UserManager $userManager;
    private FlightManager $flightManager;
    private TicketManager $ticketManager;
    private SaleManager $saleManager;

    public function __construct(MailerInterface $mailer, UserManager $userManager, FlightManager $flightManager,
                                TicketManager $ticketManager, SaleManager $saleManager) {
        $this->mailer = $mailer;
        $this->userManager = $userManager;
        $this->flightManager = $flightManager;
        $this->ticketManager = $ticketManager;
        $this->saleManager = $saleManager;
    }

    public static function getSubscribedEvents():array {
        return [
            NotificationCompleteSaleEvent::NAME => 'onCompleteSale',
            NotificationFlightCancelEvent::NAME => 'onFlightCancel'
        ];
    }

    public function onCompleteSale(NotificationCompleteSaleEvent $event)
    {
        $notification = $event->getNotification();
        $flight = $this->flightManager->flightRepository->findOneBy(['id'=>$notification->getIdFlight()]);
        if ($flight === null) {
           return false;
        }
        $tickets = $this->ticketManager->ticketRepository->findBy(['idFlight' => $flight->getId(),
                                                                   'state'=>Ticket::STATE_BOUGHT]);
        $result = [];
        foreach($tickets as $ticket){
            $sale = $this->saleManager->saleRepository->findOneBy(['idTicket'=>$ticket->getId(), 'cancellation' =>Sale::CANCELLATION_NOT]);
            $result[] = $sale->getIdUser();
        }
        foreach($result as $user){
            $user = $this->userManager->userRepository->findOneBy(['id'=>$user]);
            $email = (new TemplatedEmail())
                ->from('no-reply@aviasales.com')
                ->to($user->getEmail())
                ->subject('Продажа билетов на рейс завершена')
                ->text('Текст...');

            try {
                $this->mailer->send($email);
            } catch (TransportExceptionInterface $e) {
            }
        }
    }

    public function onFlightCancel(NotificationFlightCancelEvent $event)
    {
        $notification = $event->getNotification();
        $flight = $this->flightManager->flightRepository->findOneBy(['id'=>$notification->getIdFlight()]);
        if ($flight === null) {
            return false;
        }
        $tickets = $this->ticketManager->ticketRepository->findBy(['idFlight' => $flight->getId(),
                                                                   'state'=>Ticket::STATE_BOUGHT]);
        $result = [];
        foreach($tickets as $ticket){
            $sale = $this->saleManager->saleRepository->findOneBy(['idTicket'=>$ticket->getId(), 'cancellation' =>Sale::CANCELLATION_NOT]);
            $result[] = $sale->getIdUser();
        }
        foreach($result as $user){
            $user = $this->userManager->userRepository->findOneBy(['id'=>$user]);
            $email = (new TemplatedEmail())
                ->from('no-reply@aviasales.com')
                ->to($user->getEmail())
                ->subject('Ваш рейс отменен')
                ->text('Текст...');

            try {
                $this->mailer->send($email);
            } catch (TransportExceptionInterface $e) {
            }
        }
    }
}
